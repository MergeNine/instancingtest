﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing
{
    public class InstancePlacement
    {
        public InstancePlacement() { }

        public InstancePlacement(SpawnInfo sp)
        {
            this.Prefab = sp.Prefab;
            Position = sp.Position;
            Scale = sp.Size;
        }

        public GameObject Prefab { get; set; }
        public Vector2 Position { get; set; }
        public float Scale { get; set; }

        public Vector3 Map2DPositionToPlanePosition(Vector3 planePosition, Vector3 planeVec1, Vector3 planeVec2)
        {
            var newPoint = planePosition + planeVec1 * Position.y + planeVec2 * Position.x;
            return newPoint;
        }
    }
}
