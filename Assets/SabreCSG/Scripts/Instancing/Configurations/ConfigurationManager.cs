﻿using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing.Configurations
{
    public class ConfigurationManager
    {
        public InstancingConfiguration CurrentTemplate { get; private set; }

        public InstancingConfiguration CurrentSurfaceConfiguration { get; private set; }
         
        #region ConfigurationManagement

        public InstancingConfiguration SetInstancingConfigFromPolygon(CSGModel csgModel, Polygon poly)
        {
            CurrentSurfaceConfiguration = GetInstancingConfigFromPolygon(csgModel, poly);
            return CurrentSurfaceConfiguration;
        }


        public InstancingConfiguration GetInstancingConfigFromPolygon(CSGModel csgModel, Polygon poly)
        {
            if (csgModel == null) {
                Debug.LogWarning("CSGModel not assigned to ConfigurationManager");
                return null;
            }

            if (csgModel.BuildContext.InstancingPolygons.TryGetValue(poly.UniqueIndex, out InstancingPolygon polygon))
            {
                return polygon.Configuration;
            }
            return null;            
        }

        public void CreateConfigurationForSelectedPolys(CSGModel csgModel, List<Polygon> selectedSourcePolygons)
        {
            if (csgModel == null)
            {
                Debug.LogWarning("CSGModel not assigned to ConfigurationManager");
                return;
            }

            foreach (var selectedPoly in selectedSourcePolygons)
            {
                var instancingPolygon = new InstancingPolygon(selectedPoly, InstancingGeneratorType.BasicGrid); //TODO - allow type to be selected
                CurrentSurfaceConfiguration = instancingPolygon.Configuration;
                csgModel.BuildContext.InstancingPolygons.Add(selectedPoly.UniqueIndex, instancingPolygon);
            }
        }

        public void DeleteSelectedConfiguration(CSGModel csgModel, List<Polygon> selectedSourcePolygons)
        {
            if (CurrentSurfaceConfiguration == null)
                return;

            foreach (var selectedPoly in selectedSourcePolygons)
            {
                DeleteConfiguration(csgModel, selectedPoly);
            }
        }

        private void DeleteConfiguration(CSGModel csgModel, Polygon poly)
        {       
            csgModel.BuildContext.InstancingPolygons.Remove(poly.UniqueIndex);            
        }

        #endregion

        #region Template functions
        public void SaveNewTemplate(CSGModel csgModel, InstancingConfiguration config)
        {
            CheckConfigHasUniqueName(csgModel, config);

            if (!csgModel.BuildContext.ConfigurationTemplates.ContainsKey(config.InstanceConfigId))
            {
                csgModel.BuildContext.ConfigurationTemplates.Add(CurrentSurfaceConfiguration.InstanceConfigId, CurrentSurfaceConfiguration);
                CurrentSurfaceConfiguration.IsTemplate = true;
                CurrentTemplate = CurrentSurfaceConfiguration;
            }
            else
            {
                throw new Exception("This template is already saved!");
            }
        }

        private void CheckConfigHasUniqueName(CSGModel csgModel, InstancingConfiguration config)
        {
            foreach (var item in csgModel.BuildContext.ConfigurationTemplates)
            {
                if (item.Value.Name == config.Name && item.Key != config.InstanceConfigId)
                {
                    //TODO - PopupWindow for requesting unique name
                    throw new Exception("Template should have a unique name");
                }
            }
        }

        public void SaveExistingTemplate(CSGModel csgModel, InstancingConfiguration template)
        {
            CheckConfigHasUniqueName(csgModel, template);

            if (csgModel.BuildContext.ConfigurationTemplates.ContainsKey(template.InstanceConfigId))
            {
                csgModel.BuildContext.ConfigurationTemplates[template.InstanceConfigId] = template;
                CurrentTemplate = template;
            }
        }

        public void DeleteTemplate(CSGModel csgModel, InstancingConfiguration template)
        {
            if (csgModel.BuildContext.ConfigurationTemplates.ContainsKey(template.InstanceConfigId))
            {
                csgModel.BuildContext.ConfigurationTemplates.Remove(template.InstanceConfigId);
                CurrentTemplate = null;
            }
        }

        internal void ClearSurfaceSelection()
        {
            CurrentSurfaceConfiguration = null;
        }

        #endregion
    }
}
