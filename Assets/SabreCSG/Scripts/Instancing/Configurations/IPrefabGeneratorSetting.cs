﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing
{
    public interface IPrefabGeneratorSetting //might need to switch to abstract class
    {
        GameObject Prefab { get; set; }

        IList<InstancePlacement> Calculate2DPositions();        
    }
}
