﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing
{
    //need deep copy?
    public abstract class InstancingConfiguration
    {
        [SerializeField]
        public int InstanceConfigId;

        [SerializeField]
        public bool IsTemplate; //mostly for the GUI - 

        [SerializeField]
        public string Name; //friendly name for if this becomes a template

        public InstancingGeneratorType Type; //different ways of calculating the layout for this surface

        /// <summary>
        /// The prefabs we apply to this surface, and the settings for each
        /// </summary>
        [SerializeField] 
        public List<IPrefabGeneratorSetting> PrefabGeneratorSettingsList; //Could have various types of prefab generator setting

        public abstract IList<InstancePlacement> Calculate2DPositions(float faceLengthX, float faceLengthY);

        public InstancingConfiguration() {
            PrefabGeneratorSettingsList = new List<IPrefabGeneratorSetting>();
        }

        public abstract IPrefabGeneratorSetting AddPrefabConfiguration();

        public abstract void DrawConfigurationUI();
        
        public abstract void DrawPerPrefabConfigUI();

    }
}
