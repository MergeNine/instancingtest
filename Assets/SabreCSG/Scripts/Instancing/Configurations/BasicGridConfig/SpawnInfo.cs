﻿using Assets.SabreCSG.Scripts.Instancing;
using Assets.SabreCSG.Scripts.Instancing.Configurations.BasicGridConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class SpawnInfo 
{
            
    public Vector2 Position;
    public GameObject Prefab;
    public float Size;
    public int SizeDivider = 2; //At each level we divide the scale by this
    public int CurrentLevel = 0; //How many levels of scale we decrease by, when this is 0 we stop generating Child Positions
    public int MaxLevel = 0;
    public bool InUse;
    public List<SpawnInfo> ChildPositions;    
    public float Spacing = 0f;
    public bool IsPartialSpawnArea; //we can't build in this area, but we can in some of the children, NB: if all children are buildable, or none are, this is invalid
    private BasicGridConfiguration _config;


    public bool HasChildren => ChildPositions != null && ChildPositions.Count > 0;
    public SpawnInfo(Vector2 pos, float size, BasicGridConfiguration config) 
    {
        _config = config;
        CurrentLevel = 0;
        MaxLevel = config.MaxLevel;
        Position = pos;
        Size = size;
        InUse = false;
        ChoosePrefab();
        GenerateChildren();
    }

    private SpawnInfo(Vector2 pos, float size, int currentLevel, BasicGridConfiguration config)
    {
        if (currentLevel > config.MaxLevel)
            return;

        _config = config;
        CurrentLevel = currentLevel;
        MaxLevel = config.MaxLevel;
        Position = pos;
        Size = size; 
        InUse = false;                        
        ChoosePrefab();
        GenerateChildren();
    }

    private void ChoosePrefab()
    {

        var prefabChoices = _config.PrefabChoicesForLevel(CurrentLevel);
        if (prefabChoices != null)
        {
            Prefab = PickPrefab(prefabChoices);
            if (Prefab != null)
            {
                InUse = true;
            }
        }
    }


    private void GenerateChildren() 
    {
        ChildPositions = new List<SpawnInfo>();
        if (CurrentLevel + 1 > MaxLevel || this.InUse)
            return;

        if (SizeDivider <= 0) {
            throw new Exception("ScaleDivider cannot be 0 or less");
        }

        var childSize = Size / SizeDivider;
        var actualChildSize = childSize - Spacing;
        var startX = Position.x - Size / 2 + childSize/2;
        var startY = Position.y - Size / 2 + childSize/2;            
      
        //TODO - use prefab config to choose prefab types for each position
        for (int x = 0; x < SizeDivider; x++) {
            for (int y = 0; y < SizeDivider; y++)
            {
                var childPos = new Vector2(startX + childSize * x, startY + childSize * y);                    
                SpawnInfo childSpawn = new SpawnInfo(childPos, actualChildSize, CurrentLevel + 1, _config);                                        
                ChildPositions.Add(childSpawn);
            }
        }
    }

    private GameObject PickPrefab(List<BasicGridPrefabGeneratorSetting> prefabChoicesForLevel) 
    {
        //assumes all occurances in this list total 100
        int randomChoice = UnityEngine.Random.Range(0, 100);
        int lastNumber = 0;
        for (int i = 0; i < prefabChoicesForLevel.Count; i++) {
            if (prefabChoicesForLevel[i].Occurance > 0 && randomChoice >= lastNumber && randomChoice <= lastNumber + prefabChoicesForLevel[i].Occurance) {
                return prefabChoicesForLevel[i].Prefab;
            }
            lastNumber += prefabChoicesForLevel[i].Occurance;
        }
        return null;
    }


    public void DrawGizmos(Vector3 planePosition, Vector3 planeVec1, Vector3 planeVec2) 
    {
        var position3d = Map2DPositionToPlanePosition(planePosition, planeVec1, planeVec2);
        Gizmos.color = new Color(1f, 1/(1 + CurrentLevel), 0);
        Gizmos.DrawSphere(position3d, 0.01f*CurrentLevel);

        foreach (var child in ChildPositions) {
            child.DrawGizmos(planePosition, planeVec1, planeVec2);
        }
    }

    public Vector3 Map2DPositionToPlanePosition(Vector3 planePosition, Vector3 planeVec1, Vector3 planeVec2)
    {
        var newPoint = planePosition + planeVec1 * Position.y + planeVec2 * Position.x;
        return newPoint;
    }

    public List<Vector2> GetActivePositions() {
        List<Vector2> activePositions = new List<Vector2>();
        if (InUse)
        {
            activePositions.Add(Position);
        }
        else if (ChildPositions != null)
        {                
            foreach (var child in ChildPositions) {
                activePositions.AddRange(child.GetActivePositions());
            }
        }
        return activePositions;
    }
    
    //TODO - IsValidPartialSpawnInfo - check if we can fit at least one child in the offset space(s)
    //TODO - calculate with which child points we can build in, within offset space(s)

}
