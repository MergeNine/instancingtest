﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing.Configurations.BasicGridConfig
{
    public class OffsetParameters
    {
        public OffsetType OffsetX = OffsetType.Right;

        public OffsetType OffsetY = OffsetType.Right;

        //Note, if we reintroduce per instancing spacing, we need to account for that here
        public Vector2 CalculateOffsetFromSpacing(Vector2 totalSpacingDimentions) {
            float startXOffset = 0;
            if (OffsetX == OffsetType.Right)
            {
                startXOffset = totalSpacingDimentions.x;
            }
            else if (OffsetX == OffsetType.Centre)
            {
                startXOffset = totalSpacingDimentions.x / 2;
            }

            float startYOffset = 0;
            if (OffsetY == OffsetType.Right)
            {
                startYOffset = totalSpacingDimentions.y;
            }
            else if (OffsetY == OffsetType.Centre)
            {
                startYOffset = totalSpacingDimentions.y / 2;
            }
            return new Vector2(startXOffset, startYOffset);
        }
    }

    public enum OffsetType { 
        Left = -1,
        Centre = 0,
        Right = 1
    } 
}
