﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing
{
    [Serializable]
    public class BasicGridPrefabGeneratorSetting : IPrefabGeneratorSetting
    {

        /// <summary>
        /// The prefab we want to instance
        /// </summary>
        [SerializeField]
        public GameObject Prefab;

        /// <summary>
        /// Scale from 1 to 0, 1 maps to largest possible square length which can fit in the polygon surface
        /// </summary>
        /*[SerializeField]
        public float Scale;*/


        /// <summary>
        /// simplified way of doing scale, based on max grid scale set in main configuration
        /// </summary>
        [SerializeField]
        public int ScaleLevel; 

        /// <summary>
        /// Scale from 0 to 100
        /// </summary>
        [SerializeField]
        public int Occurance;

        GameObject IPrefabGeneratorSetting.Prefab { get =>  Prefab; set { Prefab = value; } }

        public IList<InstancePlacement> Calculate2DPositions()
        {
            throw new NotImplementedException();
        }

       

   
    }
}
