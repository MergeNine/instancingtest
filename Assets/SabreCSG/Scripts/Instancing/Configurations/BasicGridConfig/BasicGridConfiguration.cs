﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing.Configurations.BasicGridConfig
{
    public class BasicGridConfiguration : InstancingConfiguration
    {
        /// <summary>
        /// The size of the largest possible grid/square we can fit in this surface
        /// </summary>
        public float MaxGridSize { get; private set; } //cannot be larger than the smaller length of the encompassing rectangle
        
        /// <summary>
        /// The dimentions of the space we're building in
        /// </summary>
        public Vector2 SurfaceDimentions { get; private set; } //assumes a rectangle - because this is basic

        /// <summary>
        /// The space between the edges of the surface and the end of the main grid blocks,
        /// We can still build in this space however, with a modified algorithm
        /// </summary>
        public Vector2 OffsetValues { get; private set; } 
  
        /// <summary>
        /// The settings for how we move the main grid blocks in the available surface, e.g left justified, vs centre justified
        /// </summary>
        public OffsetParameters OffsetParameters { get; set; } = new OffsetParameters();

        /// <summary>
        /// The size of the prefab relative to the grid area we're placing it in
        /// </summary>
        public float PrefabScale { get; set; } = 0.5f; //TODO - pass face parameter in or face sizes

        private Dictionary<int, List<BasicGridPrefabGeneratorSetting>> _prefabConfigSortedByLevel;

        

        private void SortPrefabsByLevel() {

            _prefabConfigSortedByLevel = new Dictionary<int, List<BasicGridPrefabGeneratorSetting>>();
            
            //_config = config;
            for (int i = 0; i < PrefabGeneratorSettingsList.Count; i++)
            {
                var prefab = PrefabGeneratorSettingsList[i] as BasicGridPrefabGeneratorSetting;
                if (!_prefabConfigSortedByLevel.ContainsKey(prefab.ScaleLevel))
                {
                    _prefabConfigSortedByLevel.Add(prefab.ScaleLevel, new List<BasicGridPrefabGeneratorSetting>() { prefab });
                }
                else
                {
                    _prefabConfigSortedByLevel[prefab.ScaleLevel].Add(prefab);
                }
            }            
        }

        private int? maxLevel;
        public int MaxLevel { 
            get {
     
                return maxLevel.Value;
            } 
        }
        private void UpdateConfig() {
            maxLevel = GetHighestLevelFromPrefabConfig(PrefabGeneratorSettingsList);
            SortPrefabsByLevel();
        }

        private int GetHighestLevelFromPrefabConfig(List<IPrefabGeneratorSetting> prefabGeneratorSettingsList)
        {

            int highestLevel = 0;
            foreach (var setting in prefabGeneratorSettingsList)
            {
                var basicGridPrefabSetting = setting as BasicGridPrefabGeneratorSetting;
                if (basicGridPrefabSetting.ScaleLevel > highestLevel)
                { 
                    highestLevel = basicGridPrefabSetting.ScaleLevel;
                }
            }

            if (highestLevel > 8)
            {
                highestLevel = 8; //don't go too high, complexity is exponential 
            }
            return highestLevel;
        }

        

        public List<BasicGridPrefabGeneratorSetting> PrefabChoicesForLevel(int level)
        {
            if (_prefabConfigSortedByLevel == null) {
                SortPrefabsByLevel();
            }

            if (_prefabConfigSortedByLevel != null && _prefabConfigSortedByLevel.ContainsKey(level))
            {
                return _prefabConfigSortedByLevel[level];
            }
            return new List<BasicGridPrefabGeneratorSetting>();      
        }

        public override IList<InstancePlacement> Calculate2DPositions(float faceLengthX, float faceLengthY)
        {
            //TODO -integrate: MaxGridSize, NumberOfBlocks, spacing

            int NumberOfBlocks = 1;
            float spacing = 0.0f; //TODO -  remove spacing, since we can implement this by making the prefab slightly smaller       
            MaxGridSize = (float)CalcLargestSquareLength(faceLengthX, faceLengthY, NumberOfBlocks);
            

            if (MaxGridSize + spacing > faceLengthX)
            {
                MaxGridSize = faceLengthX - spacing;
            }
            if (MaxGridSize + spacing > faceLengthY)
            {
                MaxGridSize = faceLengthY - spacing;
            }

            SurfaceDimentions = new Vector2(faceLengthX % MaxGridSize, faceLengthY % MaxGridSize);
            int numberOfSquaresXAxis = Mathf.FloorToInt(faceLengthX / (MaxGridSize + spacing));
            int numberOfSquaresYAxis = Mathf.FloorToInt(faceLengthY / (MaxGridSize + spacing));

            List<SpawnInfo> spawnPoints = new List<SpawnInfo>();            
            OffsetValues = OffsetParameters.CalculateOffsetFromSpacing(SurfaceDimentions);
            GenerateSpawnPoints(spacing, MaxGridSize, numberOfSquaresXAxis, numberOfSquaresYAxis, spawnPoints, OffsetValues.x, OffsetValues.y);

            //TODO - generate spawnpoints in remaining space, or define size based on above config's active levels (latter is maybe better)
            
            //within Offsetspace, create one more row of SpawnInfo Objects, marked as 'partial', recurse spawnInfo generation in these, similarly to above

            List<InstancePlacement> placements = FlattenTree(spawnPoints);

            return placements;
        }
        
        private void GenerateSpawnPoints( float spacing, float squareSize, int numberOfSquaresXAxis, int numberOfSquaresYAxis, List<SpawnInfo> spawnPoints, float startX = 0, float startY = 0)
        {
            UpdateConfig();
            for (int x = 0; x < numberOfSquaresXAxis; x++)
            {
                for (int y = 0; y < numberOfSquaresYAxis; y++)
                {
                    var distX = startX + (x + 0.5) * squareSize + (x + 1) * spacing;
                    var distY = startY + (y + 0.5) * squareSize + (y + 1) * spacing;
                    Vector2 p = new Vector2((float)distX, (float)distY);
                    var spawnPoint = new SpawnInfo(p, squareSize, this);
                    spawnPoints.Add(spawnPoint);
                }
            }
            //TODO - add spawnpoints in the extra space (levels below 0)

        }

        

        private List<InstancePlacement> FlattenTree(List<SpawnInfo> spawnPoints) {
            List<InstancePlacement> placements = new List<InstancePlacement>();

            foreach (var sp in spawnPoints)
            {

                if (sp.HasChildren)
                {                    
                    var children = FlattenTree(sp.ChildPositions);
                    placements.AddRange(children);
                }
                else {
                    if (sp.InUse)
                    {
                        sp.Size = sp.Size * PrefabScale;
                        placements.Add(new InstancePlacement(sp)); 
                    }
                }               
            }
            return placements;
        }


        /// Calculates the optimal side of squares to be fit into a rectangle
        /// Inputs: x, y: width and height of the available area.
        ///         n: number of squares to fit
        /// Returns: the optimal side of the fitted squares
        double CalcLargestSquareLength(double x, double y, int n)
        {
            double sx, sy;

            var px = Math.Ceiling(System.Math.Sqrt(n * x / y));
            if (Math.Floor(px * y / x) * px < n)
            {
                sx = y / Math.Ceiling(px * y / x);
            }
            else
            {
                sx = x / px;
            }

            var py = Math.Ceiling(System.Math.Sqrt(n * y / x));
            if (Math.Floor(py * x / y) * py < n)
            {
                sy = x / Math.Ceiling(x * py / y);
            }
            else
            {
                sy = y / py;
            }

            return Math.Max(sx, sy);
        }


        public override IPrefabGeneratorSetting AddPrefabConfiguration()
        {
            var settings = new BasicGridPrefabGeneratorSetting();
            PrefabGeneratorSettingsList.Add(settings);
            return settings;
        }

        //TODO - move UI stuff into own class
        #region UI
        public override void DrawConfigurationUI() {
            //draw high level config
            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("PrefabScale", GUILayout.Width(100));
            PrefabScale = float.Parse(EditorGUILayout.TextField(PrefabScale.ToString(), GUILayout.Width(30)));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        public override void DrawPerPrefabConfigUI() {
            DrawPrefabHeadingsUI();
            
            foreach (var generatorSetting in PrefabGeneratorSettingsList)
            {
                DrawPrefabFieldsUI(generatorSetting);
            }
        }

        private void DrawPrefabFieldsUI(IPrefabGeneratorSetting settings)
        {
            if (settings is BasicGridPrefabGeneratorSetting generatorSettings)
            {
                EditorGUILayout.BeginHorizontal();
                generatorSettings.Prefab = EditorGUILayout.ObjectField(generatorSettings.Prefab, typeof(GameObject), false, GUILayout.Width(100)) as GameObject;
                GUILayout.Space(10);
                generatorSettings.ScaleLevel = int.Parse(EditorGUILayout.TextField(generatorSettings.ScaleLevel.ToString(), GUILayout.Width(100)));
                GUILayout.Space(10);
                generatorSettings.Occurance = int.Parse(EditorGUILayout.TextField(generatorSettings.Occurance.ToString(), GUILayout.Width(120)));
                EditorGUILayout.EndHorizontal();
            }
        }
   
        private void DrawPrefabHeadingsUI() //for prefab generator
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Prefab", GUILayout.Width(100));
            GUILayout.Space(10);
            EditorGUILayout.LabelField("Level (0-x)", GUILayout.Width(100));
            GUILayout.Space(10);
            EditorGUILayout.LabelField("Occurance (0-100)%", GUILayout.Width(120));
            EditorGUILayout.EndHorizontal();
        }
#endregion
    }
}
