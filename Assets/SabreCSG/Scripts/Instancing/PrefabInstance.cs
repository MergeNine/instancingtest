﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing
{

    /// <summary>
    /// These are attached to any prefab instances we generate, so we can delete these if need be
    /// </summary>
    public class PrefabInstance
    {
        [SerializeField]
        private int parentUniqueIndex;
        
    }
}
