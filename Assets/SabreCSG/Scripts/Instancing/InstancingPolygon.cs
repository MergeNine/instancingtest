﻿using Assets.SabreCSG.Scripts.Instancing.Configurations.BasicGridConfig;
using Assets.Scripts;
using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Instancing
{

    /// <summary>
    /// Stores info on how to generate prefabs on a rectangular surface
    /// </summary>
    [Serializable]
    public class InstancingPolygon : IDeepCopyable<InstancingPolygon>
    {
        /// <summary>
        /// Connection the polygons we selected to generate this polygon
        /// </summary>
        [SerializeField]
        private int uniqueIndex = -1;

        /// <summary>
        /// The vertices (see <see cref="Vertex"/>) that make up this polygonal shape.
        /// </summary>
        [SerializeField]
        private Vertex[] vertices;

        /// <summary>
        /// Surface normal, used to determine which side to place instanced prefabs on
        /// </summary>
        [SerializeField]
        public Vector3 Normal;

        [SerializeField]
        public bool IsNormalReversed;

        public int UniqueIndex
        {
            get { return uniqueIndex; }
            set { uniqueIndex = value; }
        }

        public Vertex[] Vertices
        {
            get { return vertices; }
            set { vertices = value; }
        }

        [SerializeField]
        public InstancingConfiguration Configuration;

        public InstancingPolygon() { }

        public InstancingPolygon(Polygon poly, InstancingGeneratorType type) {
            uniqueIndex = poly.UniqueIndex;
            vertices = poly.Vertices;
            Normal = poly.Plane.normal;
            if (type == InstancingGeneratorType.BasicGrid) {
                Configuration = new BasicGridConfiguration();
            }
            
        }

        public InstancingPolygon(int uniqueIndex, Vertex[] vertices, Vector3 normal, InstancingConfiguration config)
        {
            UniqueIndex = uniqueIndex;
            Vertices = vertices;
            Normal = normal;
            Configuration = config;
        }


        /// <summary>
        /// Gets the edges that represent the shape of this polygon.
        /// </summary>
        /// <returns>The edges that represent the shape of this polygon.</returns>
        public Edge[] GetEdges()
        {
            Edge[] edges = new Edge[vertices.Length];

            // iterate through all vertices:
            for (int vertexIndex1 = 0; vertexIndex1 < vertices.Length; vertexIndex1++)
            {
                // the last vertex will connect back to the first vertex to make a full circle.
                int vertexIndex2 = ((vertexIndex1 + 1) >= vertices.Length ? 0 : vertexIndex1 + 1);
                // create an edge between the current vertex and the next vertex.
                edges[vertexIndex1] = new Edge(vertices[vertexIndex1], vertices[vertexIndex2]);
            }

            return edges;
        }

        /// <summary>
        /// Gets the center point between the vertex positions of the polygon.
        /// </summary>
        /// <returns>The center point of the polygon.</returns>
        public Vector3 GetCenterPoint()
        {
            // start with the first vertex position.
            Vector3 center = vertices[0].Position;

            // add the position of all vertices to the total:
            for (int i = 1; i < vertices.Length; i++)
                center += vertices[i].Position;

            // return the average position.
            return center / vertices.Length;
        }

        /// <summary>
        /// Gets the center UV coordinates of the polygon.
        /// </summary>
        /// <returns>The center UV coordinates of the polygon</returns>
        public Vector3 GetCenterUV()
        {
            // start with the first uv position.
            Vector2 centerUV = vertices[0].UV;

            // add the uv coordinates of all vertices to the total:
            for (int i = 1; i < vertices.Length; i++)
                centerUV += vertices[i].UV;

            // normalize the average uv coordinates into 0-1 range.
            centerUV *= 1f / vertices.Length;
            return centerUV;
        }


        public bool GetPlaneVectors(out Vector3 vec1, out Vector3 vec2)
        {
            var e1 = GetEdges()[0];
            var e2 = GetEdges()[1];

            Vector3 intersectionPoint = Vector3.zero;
            Vector3 intersectionPoint2 = Vector3.zero;
            vec1 = Vector3.zero;
            vec2 = Vector3.zero;
            bool intersectionFound = false;


            //trick 1
            if (e1.Vertex1.Position == e2.Vertex1.Position || e1.Vertex1.Position == e2.Vertex2.Position)
            {
                intersectionPoint = e1.Vertex1.Position;
                intersectionFound = true;
            }

            if (e1.Vertex2.Position == e2.Vertex1.Position || e1.Vertex2.Position == e2.Vertex2.Position)
            {
                intersectionPoint = e2.Vertex1.Position;
                intersectionFound = true;
            }

            //trick 2
            var line1 = e1.Vertex2.Position - e1.Vertex1.Position;
            var line2 = e2.Vertex2.Position - e2.Vertex1.Position;            
            line1.Normalize();
            line2.Normalize();

            vec1 = line1;
            vec2 = line2;


            if (!intersectionFound && !VectorUtils.LineLineIntersection(out intersectionPoint2, e1.Vertex1.Position, line1, e2.Vertex1.Position, line2))
                return false;

            return true;
        }

        public IList<InstancePlacement> Calculate2DPositions() {

            var faceDimentions = CalculateFaceDimentions();
            return Configuration.Calculate2DPositions(faceDimentions.x, faceDimentions.y);
        }

        public Vector2 CalculateFaceDimentions()
        {
            var e1 = GetEdges()[0];
            var e2 = GetEdges()[1];

            float distanceY = Vector3.Distance(e1.Vertex1.Position, e1.Vertex2.Position);
            float distanceX = Vector3.Distance(e2.Vertex1.Position, e2.Vertex2.Position);
            return new Vector2(distanceX, distanceY); //orientation in these sizes probably doesn't matter, but this isn't really a vector2, just 2 sizes
        }

        public InstancingPolygon DeepCopy()
        {
            return new InstancingPolygon(uniqueIndex, vertices.DeepCopy(), Normal, Configuration );
        }
    }
}
