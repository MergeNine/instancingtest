﻿using Assets.SabreCSG.Scripts.Instancing;
using Assets.SabreCSG.Scripts.Instancing.Configurations;
using Assets.SabreCSG.Scripts.Tools.Utilities;
using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Tools
{
    //commented out only to test if a duplicate 'SurfaceEditor' looks the same
    
    public class InstanceEditor : Sabresaurus.SabreCSG.Tool
    {
        
        // Main UI rectangle for this tool's UI
#if UNITY_2019_3_OR_NEWER
        private readonly Rect toolbarRect = new Rect(6, 40, 215, 245);
#else
        private readonly Rect toolbarRect = new Rect(6, 40, 400, 226);
#endif
        private Rect ToolbarRect
        {
            get
            {
                Rect rect = new Rect(toolbarRect);                
                return rect;
            }
        }

        //probably we don't need this Mode enum
        private enum Mode
        { None, Translate, Rotate, QuickSelect, FollowLastFace };

        private enum GuiMode {
            NothingSelected,
            NoConfig,
            CreateConfig,
            EditTemplate,
            MultipleSelected,
            ViewTemplate
        }

        private Mode currentMode = Mode.None;
        private GuiMode currentGuiMode = GuiMode.NothingSelected;
        private Vector3 lastWorldPoint;
        private Vector3 currentWorldPoint;
        private bool pointSet = false;

        private List<Polygon> selectedSourcePolygons = new List<Polygon>();
        private Dictionary<Polygon, Brush> matchedBrushes = new Dictionary<Polygon, Brush>();

        private Polygon lastSelectedPolygon = null;
        private Polygon currentlyHighlightedPolygon = null;
        private bool debug_UsedMinimumPolyFunc = false;

        //At the moment we can only select one surface at a time
        
        //vars for CreateConfig mode
        //private InstancingConfiguration CurrentSurfaceConfiguration; //set in the configurationManager now

        //vars for edit template mode
        private InstancingConfiguration CurrentTemplate;

        private Rect alignButtonRect = new Rect(118, 110, 80, 45);

        private ConfigurationManager _configManager = new ConfigurationManager();


  

        public override void OnSceneGUI(SceneView sceneView, Event e)
        {
            base.OnSceneGUI(sceneView, e); // Allow the base logic to calculate first

            // GUI events
            OnRepaintGUI(sceneView, e);

            if (e.type == EventType.Repaint)
            {
                OnRepaint(sceneView, e);
            }
            else if (e.type == EventType.MouseMove)
            {
                SceneView.RepaintAll();
            }            
            else if (e.type == EventType.MouseDown
                && !EditorHelper.IsMousePositionInInvalidRects(e.mousePosition))
            {
                OnMouseDown(sceneView, e);
            }
            else if (e.type == EventType.MouseUp
                && !EditorHelper.IsMousePositionInInvalidRects(e.mousePosition))
            {
                OnMouseUp(sceneView, e);
            }                        

        }

        private void OnMouseUp(SceneView sceneView, Event e)
        {
            //throw new NotImplementedException();
        }

        private void OnMouseDown(SceneView sceneView, Event e)
        {
            currentlyHighlightedPolygon = GetCurrentHighlightedPolygon();

            if (currentlyHighlightedPolygon != null && SabreInput.IsModifier(e, EventModifiers.Shift))
            {
                selectedSourcePolygons.Add(currentlyHighlightedPolygon);
                currentGuiMode = GuiMode.MultipleSelected;
            }
            else if (currentlyHighlightedPolygon != null)
            {
                selectedSourcePolygons = new List<Polygon>() { currentlyHighlightedPolygon };
                _configManager.SetInstancingConfigFromPolygon(CSGModel, currentlyHighlightedPolygon);
                if (_configManager.CurrentSurfaceConfiguration == null)
                {
                    currentGuiMode = GuiMode.NoConfig;
                }
                else if (_configManager.CurrentSurfaceConfiguration.IsTemplate) {
                    currentGuiMode = GuiMode.EditTemplate;
                }
                else
                {
                    currentGuiMode = GuiMode.CreateConfig;
                }                                
            }
            else {
                _configManager.ClearSurfaceSelection();
                currentGuiMode = GuiMode.NothingSelected;
            }
        }

       

        private void OnRepaintGUI(SceneView sceneView, Event e)
        {
            // Draw UI specific to this editor
            GUIStyle toolbar = new GUIStyle(EditorStyles.toolbar);

            // Set the background tint
            if (EditorGUIUtility.isProSkin)
            {
                toolbar.normal.background = SabreCSGResources.HalfBlackTexture;
            }
            else
            {
                toolbar.normal.background = SabreCSGResources.HalfWhiteTexture;
            }
            // Set the style height to match the rectangle (so it stretches instead of tiling)
            toolbar.fixedHeight = ToolbarRect.height;
            // Draw the actual GUI via a Window
            GUILayout.Window(140009, ToolbarRect, OnToolbarGUI, "", toolbar);
        }

        private void GenerateAllPrefabs() {
            var list = CSGModel.BuildContext.InstancingPolygons.Values.ToList();
            PrefabInstancingUtility.GenerateAllPrefabs(list);
        }
        

        private void OnToolbarGUI(int windowID)
        {
            GUISkin inspectorSkin = SabreGUILayout.GetInspectorSkin();
            Rect rect = new Rect(238, 68, 60, 18);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("ReGenerate Prefabs", GUILayout.Width(140)))
            {                
                PrefabInstancingUtility.DeleteAllPrefabs();
                GenerateAllPrefabs();
            }

            if (GUILayout.Button("Clear all Instancing Config", GUILayout.Width(170)))
            {
                CSGModel.BuildContext.InstancingPolygons = new Dictionary<int, InstancingPolygon>();
                CSGModel.BuildContext.ConfigurationTemplates = new Dictionary<int, InstancingConfiguration>();
                currentGuiMode = GuiMode.NoConfig;
                PrefabInstancingUtility.DeleteAllPrefabs();
                GenerateAllPrefabs();
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            if (currentGuiMode == GuiMode.NoConfig)
            {
                DrawNoConfigToolbar();
            }
            else if (currentGuiMode == GuiMode.CreateConfig)
            {
                DrawCreateConfigToolbar();
            }
            else if (currentGuiMode == GuiMode.EditTemplate)
            {
                DrawEditTemplateToolbar();
            }
        }

        int selected = 0;

        private void DrawNoConfigToolbar() {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Current Surface: No Configuration");
            if (GUILayout.Button("Create Configuration", GUILayout.Width(140)))
            {
                _configManager.CreateConfigurationForSelectedPolys(CSGModel, selectedSourcePolygons);
                currentGuiMode = GuiMode.CreateConfig;
            }
            GUILayout.Space(20);

            //GUILayout.BeginHorizontal();
            
            string[] options = new string[]
            {
                    "Select...", "Option2", "Option3",
            };
            selected = EditorGUILayout.Popup(selected, options, GUILayout.Width(140));

            if (GUILayout.Button("Set Template", GUILayout.Width(140)))
            {
                //TODO
                currentGuiMode = GuiMode.ViewTemplate;
            }
            //GUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }


        private void DrawCreateConfigToolbar()
        {
            if (_configManager.CurrentSurfaceConfiguration == null)
                return;

            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Surface: Grid Configuration", GUILayout.Width(270));
            
            if (GUILayout.Button("Clear Surface", GUILayout.Width(110)))
            {
                _configManager.DeleteSelectedConfiguration(CSGModel, selectedSourcePolygons);
                currentGuiMode = GuiMode.NoConfig;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Name:", GUILayout.Width(35));


            _configManager.CurrentSurfaceConfiguration.Name = EditorGUILayout.TextField(_configManager.CurrentSurfaceConfiguration.Name, GUILayout.Width(100));
            
            if (GUILayout.Button("Save as Template", GUILayout.Width(130)))
            {
                //TODO - popup for name
                _configManager.SaveNewTemplate(CSGModel, _configManager.CurrentSurfaceConfiguration);                
                currentGuiMode = GuiMode.EditTemplate;
            }
            EditorGUILayout.EndHorizontal();

            _configManager.CurrentSurfaceConfiguration.DrawConfigurationUI();

            GUILayout.Space(10);
            //prefab config
            _configManager.CurrentSurfaceConfiguration.DrawPerPrefabConfigUI();

            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                //TODO - create new Prefab Generator Setting
                _configManager.CurrentSurfaceConfiguration.AddPrefabConfiguration();
            }

            EditorGUILayout.EndVertical();        
        }


        private void DrawEditTemplateToolbar()
        {

            
            if (CurrentTemplate == null)
                return;

            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField($"Template: {CurrentTemplate.Name}(Grid Configuration)", GUILayout.Width(280));
                if (GUILayout.Button("Save", GUILayout.Width(45)))
                {
                    _configManager.SaveExistingTemplate(CSGModel, CurrentTemplate);
                }
                if (GUILayout.Button("Delete ", GUILayout.Width(55)))
                {
                    _configManager.DeleteTemplate(CSGModel, CurrentTemplate);
                    currentGuiMode = GuiMode.NoConfig;
                }            
            EditorGUILayout.EndHorizontal();

            CurrentTemplate.DrawConfigurationUI();

            GUILayout.Space(10);
            //prefab config
            CurrentTemplate.DrawPerPrefabConfigUI();

            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                //TODO - create new Prefab Generator Setting
                CurrentTemplate.AddPrefabConfiguration();
            }

            EditorGUILayout.EndVertical();
        }

        private void OnRepaint(SceneView sceneView, Event e)
        {
            debug_UsedMinimumPolyFunc = false;
            // Start drawing using the relevant material
            SabreCSGResources.GetSelectedBrushMaterial().SetPass(0);
            Polygon[] allPolygons;

            // Highlight the polygon the mouse is over unless they are moving the camera, or hovering over UI
            if (currentMode == Mode.None
                && !CameraPanInProgress
                && !EditorHelper.IsMousePositionInIMGUIRect(e.mousePosition, ToolbarRect)
                && (!SabreInput.AnyModifiersSet(e) || SabreInput.IsModifier(e, EventModifiers.Control) || SabreInput.IsModifier(e, EventModifiers.Shift) || SabreInput.IsModifier(e, EventModifiers.Control | EventModifiers.Shift)))
            {
                currentlyHighlightedPolygon = GetCurrentHighlightedPolygon();

                if (currentlyHighlightedPolygon != null)
                {
                    //SabreGraphics.DrawPolygons(new Color(0, 1, 0, 0.15f), new Color(0, 1, 0, 0.5f), outlinePoly);  //simple selector                  
                    //SabreGraphics.DrawPolygons(new Color(0, 1, 0, 0.15f), new Color(0, 1, 0, 0.5f), allPolygons);  //original
                    SabreGraphics.DrawPolygons(new Color(0, 1, 0, 0.15f), new Color(0, 1, 0, 0.5f), currentlyHighlightedPolygon);
                }
            }

            SabreCSGResources.GetSelectedBrushDashedMaterial().SetPass(0);
            // Draw each of the selected polygons
            for (int i = 0; i < selectedSourcePolygons.Count; i++)
            {
                if (selectedSourcePolygons[i] != null)
                {
                    //allPolygons = csgModel.BuiltPolygonsByIndex(selectedSourcePolygons[i].UniqueIndex);
                    //SabreGraphics.DrawPolygonsNoOutline(new Color(0, 1, 0, 0.2f), allPolygons);
                    //SabreGraphics.DrawPolygonsOutlineDashed(Color.green, allPolygons);

                    SabreGraphics.DrawPolygonsNoOutline(new Color(0, 1, 0, 0.2f), selectedSourcePolygons[i]);
                    SabreGraphics.DrawPolygonsOutlineDashed(Color.green, selectedSourcePolygons[i]);
                }
            }

            // If the mouse is down draw a point where the mouse is interacting in world space
            if (e.button == 0 && pointSet)
            {
                Camera sceneViewCamera = sceneView.camera;

                SabreCSGResources.GetVertexMaterial().SetPass(0);
                GL.PushMatrix();
                GL.LoadPixelMatrix();

                GL.Begin(GL.QUADS);
                Vector3 target = sceneViewCamera.WorldToScreenPoint(currentWorldPoint);
                if (target.z > 0)
                {
                    // Make it pixel perfect
                    target = MathHelper.RoundVector3(target);
                    SabreGraphics.DrawBillboardQuad(target, 8, 8);
                }
                GL.End();
                GL.PopMatrix();
            }

            // Deselect surfaces that are not hidden during "find hidden surfaces"
            //if (findingHiddenFaces)
            //{
            //    List<Polygon> toDeselect = new List<Polygon>();
            //    for (int polygonIndex = 0; polygonIndex < selectedSourcePolygons.Count; polygonIndex++)
            //    {
            //        Polygon polygon = selectedSourcePolygons[polygonIndex];
            //        Brush brush = csgModel.FindBrushFromPolygon(polygon);

            //        if (brush.Mode == CSGMode.Add)
            //        {
            //            // is the camera on the positive side of the plane?
            //            if (Vector3.Dot(brush.transform.TransformDirection(polygon.Plane.normal), Camera.current.transform.position - brush.transform.TransformPoint(polygon.GetCenterPoint())) > 0)
            //            {
            //                // deselect polygon.
            //                toDeselect.Add(polygon);
            //            }
            //        }
            //        else if (brush.Mode == CSGMode.Subtract)
            //        {
            //            // is the camera on the positive side of the plane?
            //            if (Vector3.Dot(brush.transform.TransformDirection(polygon.Plane.normal), Camera.current.transform.position - brush.transform.TransformPoint(polygon.GetCenterPoint())) < 0)
            //            {
            //                // deselect polygon.
            //                toDeselect.Add(polygon);
            //            }
            //        }
            //    }

            //    foreach (Polygon polygon in toDeselect)
            //    {
            //        selectedSourcePolygons.Remove(polygon);
            //    }

            //    // Recalculate the matched brushes
            //    matchedBrushes.Clear();

            //    for (int i = 0; i < selectedSourcePolygons.Count; i++)
            //    {
            //        matchedBrushes.Add(selectedSourcePolygons[i], csgModel.FindBrushFromPolygon(selectedSourcePolygons[i]));
            //    }
            //}
        }

        private Polygon  GetCurrentHighlightedPolygon() {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            Polygon polygon = csgModel.RaycastBuiltPolygons(ray);
            Polygon selectedPolygon = null;
            // Hovered polygon
            if (polygon != null)
            {
                var allPolygons = csgModel.BuiltPolygonsByIndex(polygon.UniqueIndex);
                var hits = GeometryHelper.RaycastPolygonsAll(new List<Polygon>(allPolygons), ray);
                if (hits != null && hits.Count > 0)
                {
                    selectedPolygon = hits.First().Polygon;

                    if (selectedPolygon.Vertices.Length > 10)  //this is a bit arbitrary this limit
                    {
                        selectedPolygon = null;
                    }

                    if (selectedPolygon?.Vertices.Length != 4)
                    {
                        debug_UsedMinimumPolyFunc = true;
                        Debug.Log($"Polygons selected {allPolygons.Length}"); //test
                        selectedPolygon = GeometryHelper.CalculateMinimalPolygon(allPolygons);

                        if (selectedPolygon.Vertices.Length > 10)
                        {
                            selectedPolygon = null;
                        }
                        else
                        {
                            GeometryHelper.MergeColinearEdges(selectedPolygon);
                        }
                    }
                }
                else
                {
                    selectedPolygon = null;
                }
            }
 
            return selectedPolygon;
        }

        private void DeleteAllPrefabs() { 
        
        }

        private void GeneratePrefabs() { 
            
        
        }

        private void UpdateSelectedSurface() {
            //this.CSGModel.BuildContext.InstancingPolygons
        } 

        public override void Deactivated()
        {
            //throw new NotImplementedException();
        }

        public override void ResetTool()
        {
            //throw new NotImplementedException();
        }


        public override bool BrushesHandleDrawing
        {
            get
            {
                return false;
            }
        }

        public override bool PreventBrushSelection
        {
            get
            {
                // Can't select brushes in the scene view in the Face tool
                return true;
            }
        }
    }
}
