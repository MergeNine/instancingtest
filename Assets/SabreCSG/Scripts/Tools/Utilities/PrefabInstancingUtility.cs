﻿using Assets.SabreCSG.Scripts.Instancing;
using Assets.Scripts;
using Assets.Scripts.Utils;
using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.SabreCSG.Scripts.Tools.Utilities
{
    public class PrefabInstancingUtility
    {

        public static void DeleteAllPrefabs()
        {
            var prefabs = GameObject.FindGameObjectsWithTag("generated");

            foreach (var item in prefabs)
            {
                if (Application.isPlaying)
                    GameObject.Destroy(item);
                else
                    GameObject.DestroyImmediate(item);
            }
        }

       
        public static void GenerateAllPrefabs(List<InstancingPolygon> surfaces) {

            foreach (var polygon in surfaces) {
                GenerateSurfacePrefabs2(polygon);
            }        
        }


        public static void GenerateSurfacePrefabs2(InstancingPolygon surface) {

            if (surface == null || surface.Configuration == null) {
                return;
            }

            //TODO - validation                     
            IList<InstancePlacement> spawnPositions2D = surface.Calculate2DPositions();
            Vector3 planeVec1;
            Vector3 planeVec2;

            bool vectorsIntersect = surface.GetPlaneVectors(out planeVec1, out planeVec2); //for now, assumes a rectangular plane

            if (!vectorsIntersect) {
                throw new Exception("Not a plane, given vectors don't intersect");
            }

            planeVec1.Normalize();
            planeVec2.Normalize();

            Debug.DrawRay(surface.GetCenterPoint(), surface.Normal, Color.red);

            var edge1 = surface.GetEdges()[0];
            var edge2 = surface.GetEdges()[1];
            var midPoint = surface.GetCenterPoint();// edge1.Vertex1.Position + planeVec1 * faceDimentions.y / 2 + planeVec2 * faceDimentions.x / 2;

            foreach (var p in spawnPositions2D)
            {
                var spawnPoint = p.Map2DPositionToPlanePosition(edge1.Vertex1.Position, planeVec1, planeVec2); 
                var direction = Quaternion.FromToRotation(Vector3.up, surface.Normal);

                //TODO - sort out scale, could use our own script on the prefab to make this fit better
                var prefabMesh = p.Prefab.gameObject.transform.GetComponent<MeshFilter>();
                var offset = CalculatePrefabSurfaceOffset(p, surface.Normal);

                var obj = GameObject.Instantiate(p.Prefab, offset + spawnPoint, Quaternion.identity);                
                obj.transform.localRotation = obj.transform.localRotation * direction;
                obj.tag = "generated";
                obj.transform.localScale = new Vector3(p.Scale, p.Scale, p.Scale); 

                //p.DrawGizmos(edge1.Vertex1.Position, planeVec1, planeVec2);
            }
        }

        private static Vector3 CalculatePrefabSurfaceOffset(InstancePlacement placement, Vector3 surfaceNormal) {
            var prefabMesh = placement.Prefab.gameObject.transform.GetComponent<MeshFilter>();

            if (prefabMesh != null) {
                var offset = prefabMesh.sharedMesh.bounds.extents.y * placement.Scale * surfaceNormal;
                return offset;
            } else  {
                throw new Exception("Prefab must have Mesh Filter Component");
            }            
        }


        private void DrawDebugInfo(List<InstancingPolygon> polygons) {
            foreach (var p in polygons)
            {
                foreach (var v in p.Vertices)
                {
                    DebugDrawer.Instance.AddPoint(v.Position, Color.black);
                }
            }
        }
        


        //TODO - try converting this to c# to resolve rotation issue
        /*
        Quaternion getRotationQuat(const Vector& from, const Vector& to){          
            Quaternion result; 
        Vector H = VecAdd(from, to); 
        H = VecNormalize(H); 
        result.w = VecDot(from, H); 
        result.x = from.y* H.z - from.z* H.y; result.y = from.z* H.x - from.x* H.z; 
        result.z = from.x* H.y - from.y* H.x;         
        return result;
    }
    */

        
    




  

    }
}
