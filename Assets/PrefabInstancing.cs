﻿using Assets.Scripts;
using Assets.Scripts.Utils;
using Sabresaurus.SabreCSG;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This is redundant code and will be deleted soon
/// </summary>
public partial class PrefabInstancing : MonoBehaviour
{
    public GameObject Prefab;
    public Material MarkerMaterial;

    public int NumberOfBlocks;

    public float SquareSize = 5f;
    public float SquareSpacing = 0.1f; //percentage of space used as spacing, at each level
    public int Levels = 2;

    public float PrefabScale = 1f;

    public bool Generate = false;
    private bool _generate = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void TestMeshCalculations() {

        //Test1();
    }


    private void Test1() {
        //test
        var mesh = new Mesh();
        List<Vector3> vertices = new List<Vector3>();
        vertices.Add(new Vector3(0, 0, 0));
        vertices.Add(new Vector3(2, 0, 0));
        vertices.Add(new Vector3(2, 2, 0));
        vertices.Add(new Vector3(0, 2, 0));

        vertices.Add(new Vector3(0, 0, 4));
        vertices.Add(new Vector3(2, 0, 4));
        vertices.Add(new Vector3(2, 2, 4));
        vertices.Add(new Vector3(0, 2, 4));
        mesh.vertices = vertices.ToArray();
        //

        bool ans1 = MeshCalculations.PointIsWithin(mesh, new Vector3(-1,0,0));
        bool ans2 = MeshCalculations.PointIsWithin(mesh, new Vector3(1, 1, 1));
        bool ans3 = MeshCalculations.PointIsWithin(mesh, new Vector3(1.99f, 0.001f, 0.001f));
        bool ans4 = MeshCalculations.PointIsWithin(mesh, new Vector3(2, 2, 4));

        Debug.LogWarning($"Answer1: {ans1}");
        Debug.LogWarning($"Answer2: {ans2}");
        Debug.LogWarning($"Answer3: {ans3}");
        Debug.LogWarning($"Answer4: {ans4}");
    }

    private void OnDrawGizmos()
    {

        //TODO - move this all into proper class
        if (Generate != _generate)
        {
            TestMeshCalculations();

            GeneratePrefabs();
            _generate = Generate;
        }
        //DrawTHeGizmos();
    }

    public void DeleteAllPrefabs() {
        var prefabs = GameObject.FindGameObjectsWithTag("generated");

        foreach (var item in prefabs) {
            if (Application.isPlaying)
                GameObject.Destroy(item);
            else
                GameObject.DestroyImmediate(item);
        }
    }

    
    public void DrawTHeGizmos()
    {
        
        var thing = gameObject.GetComponent<CSGModel>();
        var brushes = thing.GetBrushes();
        var generatedMesh = gameObject.GetComponentsInChildren<MeshFilter>();

        foreach (var m in generatedMesh) {
            foreach (var v in m.mesh.vertices)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(v, 0.2f);
            }
        }

            DeleteAllPrefabs();

            foreach (var b in brushes)
            {
                var polygons = b.GenerateTransformedPolygons();

                foreach (var poly in polygons)
                {
                    if (poly.Material != null && MarkerMaterial != null && MarkerMaterial.mainTexture.name == poly.Material.mainTexture.name)
                    {
                        var faceDimentions = CalculateFaceDimentions(poly.GetEdges());
                        var spawnPoints2d = Calculate2DSpawnPositions(faceDimentions.x, faceDimentions.y, SquareSize, SquareSpacing); //TODO - Calculate Spawn Positions and scales on Face in 2D

                        Vector3 planeVec1;
                        Vector3 planeVec2;
                        GetPlaneVectors(poly, out planeVec1, out planeVec2); //for now, assumes a rectangular plane
                        planeVec1.Normalize();
                        planeVec2.Normalize();

                        var edge1 = poly.GetEdges()[0];
                        var edge2 = poly.GetEdges()[1];
                        var midPoint = edge1.Vertex1.Position + planeVec1 * faceDimentions.y / 2 + planeVec2 * faceDimentions.x / 2;

                        //foreach (var p in spawnPoints2d)
                        //{
                        //    var spawnPoint = p.Map2DPositionToPlanePosition(edge1.Vertex1.Position, planeVec1, planeVec2);
                        //    var direction = Quaternion.FromToRotation(Vector3.up, -poly.Plane.normal);
                        //    var obj = GameObject.Instantiate(Prefab, spawnPoint, direction);
                        //    obj.tag = "generated";
                        //    obj.transform.localScale = new Vector3(PrefabScale, PrefabScale, PrefabScale);

                        //    p.DrawGizmos(edge1.Vertex1.Position, planeVec1, planeVec2);
                        //}

                        //TODO - Map 2D positions to 3d space
                        //TODO - Spawn prefabs to this surface
                        var centrePoint = poly.GetCenterPoint();
                        var dir = poly.Plane.normal;

                        var bounds = poly.GetBounds();
                        var edges = poly.GetEdges();

                        Gizmos.color = Color.green;
                        Gizmos.DrawSphere(bounds.center, 0.2f);
                        Gizmos.DrawSphere(bounds.min, 0.1f);
                        Gizmos.DrawSphere(bounds.max, 0.1f);

                        Gizmos.color = Color.yellow;
                        Gizmos.DrawSphere(midPoint, 0.2f);

                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(centrePoint, centrePoint + (4 * dir));
                    }
                }
            }
        
    }

    public int testId= 1;

    public void GeneratePrefabs() {
        DebugDrawer.Instance.ClearPoints();
        var csgModel = gameObject.GetComponent<CSGModel>();
        var brushes = csgModel.GetBrushes();

        var polysToDraw = csgModel.BuiltPolygonsByIndex(testId);

        foreach (var p in polysToDraw) {
            foreach (var v in p.Vertices) {
                DebugDrawer.Instance.AddPoint(v.Position, Color.black);
            }
        }

        return;//test

        DeleteAllPrefabs();

        foreach (var b in brushes)
        {
            
            //var polygons = b.GenerateTransformedPolygons(); //brush before cut
            var polygons = b.BrushCache.BuiltVisualPolygons; //tesselated polygons
            if (b.Mode == CSGMode.Add)
            {
                

                foreach (var poly in polygons)
                {
                    if (poly.Material != null && MarkerMaterial != null && MarkerMaterial.mainTexture.name == poly.Material.mainTexture.name)
                    {
                        var faceDimentions = CalculateFaceDimentions(poly.GetEdges());
                        var spawnPoints2d = Calculate2DSpawnPositions(faceDimentions.x, faceDimentions.y, SquareSize, SquareSpacing); //TODO - Calculate Spawn Positions and scales on Face in 2D
                        

                        Vector3 planeVec1;
                        Vector3 planeVec2;
                        GetPlaneVectors(poly, out planeVec1, out planeVec2); //for now, assumes a rectangular plane
                        planeVec1.Normalize();
                        planeVec2.Normalize();

                        Debug.DrawRay(poly.GetCenterPoint(), poly.Plane.normal, Color.red);

                        var edge1 = poly.GetEdges()[0];
                        var edge2 = poly.GetEdges()[1];
                        var midPoint = edge1.Vertex1.Position + planeVec1 * faceDimentions.y / 2 + planeVec2 * faceDimentions.x / 2;

                        foreach (var p in spawnPoints2d)
                        {
                            var spawnPoint = p.Map2DPositionToPlanePosition(edge1.Vertex1.Position, planeVec1, planeVec2);
                            //var direction = Quaternion.LookRotation(poly.Plane.normal, Vector3.forward);

                            var direction = Quaternion.FromToRotation(Vector3.up, -poly.Plane.normal);
                            var obj = GameObject.Instantiate(Prefab, spawnPoint, Quaternion.identity);
                            obj.transform.localRotation = obj.transform.localRotation * direction;
                            obj.tag = "generated";
                            obj.transform.localScale = new Vector3(PrefabScale, PrefabScale, PrefabScale);

                            //p.DrawGizmos(edge1.Vertex1.Position, planeVec1, planeVec2);
                        }

  
                        //TODO - Map 2D positions to 3d space
                        //TODO - Spawn prefabs to this surface
                        /*var centrePoint = poly.GetCenterPoint();
                        var dir = poly.Plane.normal;

                        var bounds = poly.GetBounds();
                        var edges = poly.GetEdges();

                        Gizmos.color = Color.green;
                        Gizmos.DrawSphere(bounds.center, 0.2f);
                        Gizmos.DrawSphere(bounds.min, 0.1f);
                        Gizmos.DrawSphere(bounds.max, 0.1f);

                        Gizmos.color = Color.yellow;
                        Gizmos.DrawSphere(midPoint, 0.2f);

                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(centrePoint, centrePoint + (4 * dir));*/
                    }
                }
            }
        }
    }

    //TODO - try converting this to c# to resolve rotation issue
    /*
    Quaternion getRotationQuat(const Vector& from, const Vector& to){          
        Quaternion result; 
    Vector H = VecAdd(from, to); 
    H = VecNormalize(H); 
    result.w = VecDot(from, H); 
    result.x = from.y* H.z - from.z* H.y; result.y = from.z* H.x - from.x* H.z; 
    result.z = from.x* H.y - from.y* H.x;         
    return result;
}
*/
private Vector2 CalculateFaceDimentions(Edge[] edges) {

        float distanceY = Vector3.Distance(edges[0].Vertex1.Position, edges[0].Vertex2.Position);
        float distanceX = Vector3.Distance(edges[1].Vertex1.Position, edges[1].Vertex2.Position);
        return new Vector2(distanceX, distanceY); //orientation in these sizes probably doesn't matter, but this isn't really a vector2, just 2 sizes

    }

    List<SpawnInfo> Calculate2DSpawnPositions(float faceLengthX, float faceLengthY, float spacing) {
        float bigSquareSize = (float)CalcLargestSquareLength(faceLengthX, faceLengthY, NumberOfBlocks);
        float totalSpacingX = faceLengthX % bigSquareSize;       
        float totalSpacingY = faceLengthY % bigSquareSize;

        if (bigSquareSize + spacing > faceLengthX)
            bigSquareSize = faceLengthX - spacing;

        if (bigSquareSize + spacing > faceLengthY)
            bigSquareSize = faceLengthY - spacing;

        int numberOfSquaresXAxis = Mathf.RoundToInt(faceLengthX / (bigSquareSize + spacing));
        int numberOfSquaresYAxis = Mathf.RoundToInt(faceLengthY / (bigSquareSize + spacing));

        List<SpawnInfo> spawnPoints = new List<SpawnInfo>();
        for (int x = 0; x < numberOfSquaresXAxis; x++) 
        {
            for (int y = 0; y < numberOfSquaresYAxis; y++)
            {
                var distX =  (x + 0.5) * bigSquareSize + (x + 1) * spacing;
                var distY =  (y + 0.5) * bigSquareSize + (y + 1) * spacing;
                Vector2 p = new Vector2((float)distX, (float)distY);
                //var spawnPoint = new SpawnInfo(p, bigSquareSize, 0, Levels);
                //spawnPoints.Add(spawnPoint);
            }
        }

        return spawnPoints;
    }

    //TODO - setup recursive calculation

    //should replace single point already calculated



    List<SpawnInfo> Calculate2DSpawnPositions(float faceLengthX, float faceLengthY, float bigSquareSize = 1f, float innerSpacing = 0.01f)
    {
        if ((bigSquareSize + innerSpacing) > faceLengthX)
            bigSquareSize = faceLengthX - innerSpacing;

        if ((bigSquareSize + innerSpacing) > faceLengthY)
            bigSquareSize = faceLengthY - innerSpacing;
        
        float outerSpacingX = faceLengthX % (bigSquareSize + innerSpacing);
        float outerSpacingY = faceLengthY % (bigSquareSize + innerSpacing);

        int numberOfSquaresXAxis = Mathf.FloorToInt(faceLengthX / (bigSquareSize + innerSpacing));
        int numberOfSquaresYAxis = Mathf.FloorToInt(faceLengthY / (bigSquareSize + innerSpacing));
        
        List<SpawnInfo> spawnPoints = new List<SpawnInfo>();
        for (int x = 0; x < numberOfSquaresXAxis; x++)
        {
            for (int y = 0; y < numberOfSquaresYAxis; y++)
            {
                var distX = outerSpacingX/2 + (bigSquareSize + innerSpacing) * x + 0.5 * bigSquareSize;
                var distY = outerSpacingY/2 + (bigSquareSize + innerSpacing) * y + 0.5 * bigSquareSize;
                
                Vector2 p = new Vector2((float)distX, (float)distY);
                //spawnPoints.Add(new SpawnInfo(p, bigSquareSize, 0, Levels));
            }
        }

        return spawnPoints;
    }

    /// Calculates the optimal side of squares to be fit into a rectangle
    /// Inputs: x, y: width and height of the available area.
    ///         n: number of squares to fit
    /// Returns: the optimal side of the fitted squares
    double CalcLargestSquareLength(double x, double y, int n)
    {
        double sx, sy;

        var px = Math.Ceiling(System.Math.Sqrt(n * x / y));
        if (Math.Floor(px * y / x) * px < n)
        {
            sx = y / Math.Ceiling(px * y / x);
        }
        else
        {
            sx = x / px;
        }

        var py = Math.Ceiling(System.Math.Sqrt(n * y / x));
        if (Math.Floor(py * x / y) * py < n)
        {
            sy = x / Math.Ceiling(x * py / y);
        }
        else
        {
            sy = y / py;
        }

        return Math.Max(sx, sy);
    }







    //WIP
    private bool GetPlaneVectors(Polygon poly, out Vector3 vec1, out Vector3 vec2) 
    {
        var e1 = poly.GetEdges()[0];
        var e2 = poly.GetEdges()[1];

        Vector3 intersectionPoint = Vector3.zero;
        Vector3 intersectionPoint2 = Vector3.zero;
        vec1 = Vector3.zero;
        vec2 = Vector3.zero;

        /*if (!e1.Intersects(e2))
            return false;*/

        //trick 1
        if (e1.Vertex1.Position == e2.Vertex1.Position || e1.Vertex1.Position == e2.Vertex2.Position)
            intersectionPoint = e1.Vertex1.Position;

        if (e1.Vertex2.Position == e2.Vertex1.Position || e1.Vertex2.Position == e2.Vertex2.Position)
            intersectionPoint = e2.Vertex1.Position;

        //trick 2
        var line1 = e1.Vertex2.Position - e1.Vertex1.Position;
        var line2 = e2.Vertex2.Position - e2.Vertex1.Position;
        
        if (!VectorUtils.LineLineIntersection(out intersectionPoint2, e1.Vertex1.Position, line1, e2.Vertex1.Position, line2))
            return false;

        vec1 = line1;
        vec2 = line2;

        return true;
    }




    public float CalculatePrefabNormal() {
        throw new NotImplementedException();
    }

}
