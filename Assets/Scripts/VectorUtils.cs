﻿using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class VectorUtils
    {
        static double Threshold = Double.Parse("1.0E-10");
            
            

        public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
        {

            Vector3 lineVec3 = linePoint2 - linePoint1;
            Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
            Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

            float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

            //is coplanar, and not parallel
            if (FastApproximately(planarFactor, 0f, (float)Threshold) && !Mathf.Approximately(crossVec1and2.sqrMagnitude, 0f))
            {
                float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
                intersection = linePoint1 + (lineVec1 * s);
                return true;
            }
            else
            {
                //ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, linePoint1, lineVec1, linePoint2, lineVec2);

                intersection = Vector3.zero;
                return false;
            }
        }

        public static bool FastApproximately(float a, float b, float threshold)
        {
            if (threshold > 0f)
            {
                return Mathf.Abs(a - b) <= threshold;
            }
            else
            {
                return Mathf.Approximately(a, b);
            }
        }


        //Two non-parallel lines which may or may not touch each other have a point on each line which are closest
        //to each other. This function finds those two points. If the lines are not parallel, the function 
        //outputs true, otherwise false.
        public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
        {

            closestPointLine1 = Vector3.zero;
            closestPointLine2 = Vector3.zero;

            float a = Vector3.Dot(lineVec1, lineVec1);
            float b = Vector3.Dot(lineVec1, lineVec2);
            float e = Vector3.Dot(lineVec2, lineVec2);

            float d = a * e - b * b;

            //lines are not parallel
            if (d != 0.0f)
            {

                Vector3 r = linePoint1 - linePoint2;
                float c = Vector3.Dot(lineVec1, r);
                float f = Vector3.Dot(lineVec2, r);

                float s = (b * f - c * e) / d;
                float t = (a * f - c * b) / d;

                closestPointLine1 = linePoint1 + lineVec1 * s;
                closestPointLine2 = linePoint2 + lineVec2 * t;

                return true;
            }

            else
            {
                return false;
            }
        }



        //https://answers.unity.com/questions/1522620/converting-a-3d-polygon-into-a-2d-polygon.html
        public static List<Vector2> Convert3DPlanePolygonTo2D(Vector3 planeNormal, List<Vector3> polygonPoints)
        {
            Vector3 u;
            if (Mathf.Abs(Vector3.Dot(Vector3.forward, planeNormal)) < 0.2f)
                u = Vector3.ProjectOnPlane(Vector3.right, planeNormal);
            else
                u = Vector3.ProjectOnPlane(Vector3.forward, planeNormal);


            Vector3 v = Vector3.Cross(u, planeNormal).normalized;

            List<Vector2> newPoints = new List<Vector2>();

            foreach (var p in polygonPoints)
            {
                newPoints.Add(new Vector2(Vector3.Dot(p, u), Vector3.Dot(p, v)));
            }

            return newPoints;
        }

        public static List<Vector3> GetPointsFromPoly(Polygon poly) {
            List<Vector3> polyPoints = new List<Vector3>();
            foreach (var v in poly.Vertices)
                polyPoints.Add(v.Position);

            return polyPoints;
        }

        public static bool Approximately(Vector3 a, Vector3 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y) && Mathf.Approximately(a.z, b.z);
        }

        public static bool Approximately(Vector2 a, Vector2 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
        }
    }

}
