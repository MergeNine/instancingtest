﻿using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// For a given 3D Convex flat polygon surface, this algorithm finds the largest possible rectangle which can fit inside it.
    /// Uses algorithm defined at: https://pdfs.semanticscholar.org/701f/eacc257d10da846bd2825ffff2f3322a1d11.pdf
    /// </summary>
    public class LargestRectangleInPolygon
    {

        public LargestRectangleInPolygon(Polygon poly) {
            //TODO - convert 3d surface to 2d

            //TODO - set angle of 2d surface (0-360)
        }

     



        //TODO -  divide polygon into subareas based on vertices

        //TODO - Identify subareas

        //TODO - Form RA Graph

        //TODO - Identify Cycles and Paths so to get the largest rectangle
    }


}
