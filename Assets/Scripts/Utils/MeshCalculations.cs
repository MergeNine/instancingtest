﻿using Sabresaurus.SabreCSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class MeshCalculations
    {

        public static Bounds CalculateBoundingBox(GameObject aObj)
        {
            if (aObj == null)
            {
                Debug.LogError("CalculateBoundingBox: object is null");
                return new Bounds(Vector3.zero, Vector3.one);
            }
            Transform myTransform = aObj.transform;
            Mesh mesh = null;
            MeshFilter mF = aObj.GetComponent<MeshFilter>();
            if (mF != null)
                mesh = mF.mesh;
            else
            {
                SkinnedMeshRenderer sMR = aObj.GetComponent<SkinnedMeshRenderer>();
                if (sMR != null)
                    mesh = sMR.sharedMesh;
            }
            if (mesh == null)
            {
                Debug.LogError("CalculateBoundingBox: no mesh found on the given object");
                return new Bounds(aObj.transform.position, Vector3.one);
            }
            Vector3[] vertices = mesh.vertices;
            if (vertices.Length <= 0)
            {
                Debug.LogError("CalculateBoundingBox: mesh doesn't have vertices");
                return new Bounds(aObj.transform.position, Vector3.one);
            }
            Vector3 min, max;
            min = max = myTransform.TransformPoint(vertices[0]);
            for (int i = 1; i < vertices.Length; i++)
            {
                Vector3 V = myTransform.TransformPoint(vertices[i]);
                for (int n = 0; n < 3; n++)
                {
                    if (V[n] > max[n])
                        max[n] = V[n];
                    if (V[n] < min[n])
                        min[n] = V[n];
                }
            }
            Bounds B = new Bounds();
            B.SetMinMax(min, max);
            return B;
        }


        public static bool PointIsWithinBoundingSphere(this Mesh mesh, Vector3 point)
        {
            if (!mesh.bounds.Contains(point))
                return false;

            // Get the point relative to the mesh center.
            var p = point - mesh.bounds.center;
            Func<float, float> getSide = vector => vector < 0f ? -1f : 1f;
            var sideFlip = new Vector3(getSide(p.x), getSide(p.y), getSide(p.z));
            float pointDist = p.sqrMagnitude;
            foreach (var vert in mesh.vertices)
            {
                // Get the vertice relative to the mesh center.
                var v = vert - mesh.bounds.center;
                // Is vertice on other side of mesh center than the target point?
                if (v.x * sideFlip.x < 0f || v.y * sideFlip.y < 0f || v.z * sideFlip.z < 0f)
                    continue;
                float vectorDist = v.sqrMagnitude;
                if (vectorDist > pointDist)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Decide whether a point is within a mesh, in a good yet simplistic way.
        /// It works best with convex meshes, whereas a concave mesh is simply
        /// treated as a convex mesh and so it will not be totally exact.
        /// (For a torus/donut mesh it will be like it didn't have a hole.)
        /// </summary>
        public static bool PointIsWithin(this Mesh mesh, Vector3 point)
        {
            Vector3 p = point - mesh.bounds.center;

            for (int i = 0; i < mesh.vertices.Length - 2; i += 3)
            {
                Vector3 a = mesh.vertices[i] - mesh.bounds.center;
                Vector3 b = mesh.vertices[i + 1] - mesh.bounds.center;
                Vector3 c = mesh.vertices[i + 2] - mesh.bounds.center;
                if (RayWithinTriangle(p, a, b, c))
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Radiate out from the origin through the given point to see whether
        /// the ray would hit the triangle and the point is closer to the origin than the triangle.
        /// The triangle is specified by v0, v1 and v2.
        /// </summary>
        private static bool RayWithinTriangle(Vector3 point, Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 intersectionPoint;
            if (RayIntersectsTriangle(point, v0, v1, v2, out intersectionPoint))
            {
                float pointDist = point.sqrMagnitude;
                float intersectionDist = intersectionPoint.sqrMagnitude;
                return (pointDist < intersectionDist);
            }
            return false;
        }

        /// <summary>
        /// Radiate out from the origin through the given point to see whether
        /// the ray would hit the triangle.
        /// The triangle is specified by v0, v1 and v2.
        /// </summary>
        private static bool RayIntersectsTriangle(Vector3 direction, Vector3 v0, Vector3 v1, Vector3 v2, out Vector3 intersectionPoint)
        {
            intersectionPoint = new Vector3();

            Vector3 e1 = v1 - v0;
            Vector3 e2 = v2 - v0;

            Vector3 h = Vector3.Cross(direction, e2);
            float a = Vector3.Dot(e1, h);

            if (a > -0.00001 && a < 0.00001)
                return false;

            float f = 1 / a;
            Vector3 s = Vector3.zero - v0;
            float u = f * Vector3.Dot(s, h);

            if (u < 0.0 || u > 1.0)
                return false;

            Vector3 q = Vector3.Cross(s, e1);
            float v = f * Vector3.Dot(direction, q);

            if (v < 0.0 || u + v > 1.0)
                return false;

            // At this stage we can compute t to find out where
            // the intersection point is on the line.
            float t = f * Vector3.Dot(e2, q);

            if (t > 0.00001) // ray intersection
            {
                intersectionPoint[0] = direction[0] * t;
                intersectionPoint[1] = direction[1] * t;
                intersectionPoint[2] = direction[2] * t;
                return true;
            }

            // At this point there is a line intersection
            // but not a ray intersection.
            return false;
        }

    }
}
