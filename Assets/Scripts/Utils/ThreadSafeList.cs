﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Utils
{
    public class ThreadSafeList<T> : IList<T>
    {
        protected List<T> _interalList = new List<T>();

        // Other Elements of IList implementation

        public IEnumerator<T> GetEnumerator()
        {
            return Clone().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Clone().GetEnumerator();
        }

        protected static object _lock = new object();

        public int Count {
            get {               
                return Clone().Count;                
            }
        }
        

        public bool IsReadOnly => throw new NotImplementedException();

        public T this[int index]
        {
            get
            {          
                return Clone()[index];                
            }

            set
            {
                lock (_lock)
                {
                    _interalList[index] =  value;
                }
            }
        }

        public List<T> Clone()
        {
            List<T> newList = new List<T>();

            lock (_lock)
            {
                _interalList.ForEach(x => newList.Add(x));
            }

            return newList;
        }

        public int IndexOf(T item)
        {
            return Clone().IndexOf(item);            
        }

        public void Insert(int index, T item)
        {
            lock (_lock)
            {
                _interalList.Insert(index, item);
            }
        }

        public void RemoveAt(int index)
        {
            lock (_lock)
            {
                _interalList.RemoveAt(index);
            }
        }

        public void Add(T item)
        {
            lock (_lock)
            {
                _interalList.Add(item);
            }
        }

        public void Clear()
        {
            lock (_lock)
            {
                _interalList.Clear();
            }
        }

        public bool Contains(T item)
        {
            return Clone().Contains(item);            
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (_lock)
            {
                _interalList.CopyTo(array, arrayIndex);
            }
        }

        public bool Remove(T item)
        {
            lock (_lock)
            {
                return _interalList.Remove(item);
            }
        }
    }

// https://stackoverflow.com/questions/9995266/how-to-create-a-thread-safe-generic-list
    /*class MyList<T>
    {
        private List<T> _list = new List<T>();
        private object _sync = new object();
        public void Add(T value)
        {
            lock (_sync)
            {
                _list.Add(value);
            }
        }
        public T Find(Predicate<T> predicate)
        {
            lock (_sync)
            {
                return _list.Find(predicate);
            }
        }
        public T FirstOrDefault()
        {
            lock (_sync)
            {
                return _list.FirstOrDefault();
            }
        }
    }*/
}
