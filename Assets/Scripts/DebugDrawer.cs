﻿using Assets.Scripts.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Assets.Scripts
{
    public class DebugDrawer : MonoBehaviour
    {

        ThreadSafeList2<PointInfo> _PointsToDraw = new ThreadSafeList2<PointInfo>();
        /*Dictionary<int, Color> _colours = new Dictionary<int, Color>();*/


        class PointInfo {
            public Vector3 Position;
            public Color Colour;
        }

        private static DebugDrawer m_Instance = null;
        public static DebugDrawer Instance
        {
            get
            {
                
                if (m_Instance == null)
                {
                    Debug.Log("Instantiating debug drawer");
                    m_Instance = FindObjectOfType<DebugDrawer>();
                    // fallback, might not be necessary.
                    if (m_Instance == null)
                        m_Instance = new GameObject(typeof(DebugDrawer).Name).AddComponent<DebugDrawer>();
#if !UNITY_EDITOR
                    DontDestroyOnLoad(m_Instance.gameObject);
#endif 
                }
                return m_Instance;
            }
        }



        public void ClearPoints() {
            _PointsToDraw.Clear(); 
        }

        /*public void AddPoint(Vector3 newPoint) {
            
            if (_PointsToDraw.Count == 0)
            {
                _PointsToDraw.Add(new PointInfo() { Position = newPoint });
                return;
            }

            foreach (var p in _PointsToDraw) {
                if (!Approximately(newPoint, p))
                {
                    _PointsToDraw.Add(new PointInfo() { Position = newPoint });
                }
            }
            
        }*/

        public void AddPoint(Vector3 newPoint) {
            AddPoint(newPoint, Color.grey);
        }

        public void AddPoint(Vector3 newPoint, Color col)
        {
            try
            {

                

                //Debug.Log($"point count {_PointsToDraw.Count}");

                PointInfo entry = _PointsToDraw.FirstOrDefault(x => VectorUtils.Approximately(x.Position, newPoint));

                if (entry == null)
                {
                    _PointsToDraw.Add(new PointInfo()
                    {
                        Position = newPoint,
                        Colour = col
                    });
                }
                else {
                    Debug.LogWarning($"trying to add duplicate Point {entry.Position}");
                }
            } catch (Exception ex) {
                Debug.LogError($"AddPoint{ex.Message}");
            }
        }




        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            foreach (var p in _PointsToDraw)
            { 
                Gizmos.color = p.Colour;
                Gizmos.DrawSphere(p.Position, 0.1f);
            }
        }
    }

}
